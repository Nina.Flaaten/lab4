package gui;

import cellular.BriansBrain;
import cellular.CellAutomaton;
import cellular.GameOfLife;

public class Main {

	public static void main(String[] args) {
		CellAutomaton ca = new GameOfLife(100,100); //for å kjøre GameOfLife
		CellAutomaton cb = new BriansBrain(100, 100); //for å kjøre BriansBrain
		CellAutomataGUI.run(ca);
		CellAutomataGUI.run(cb);
	}

}



// husk: flere egne kommentarer i OneNote