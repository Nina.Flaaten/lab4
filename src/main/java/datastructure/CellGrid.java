package datastructure;
import cellular.CellState;


public class CellGrid implements IGrid {
    // TODO Legg inn feltvariablene som mangler i CellGrid...
    
    int rows;             // høydeverdi
    int cols;          // breddeverdi
    CellState[][] grid;   // todimensjonal datastruktur
    

    // Constructor
    public CellGrid(int rows, int cols, CellState initialState) {
		// TODO ...assign verdier til dem i CellGrid-konstruktøren.
        this.rows = rows;
        this.cols = cols;
        this.grid = new CellState[rows][cols]; 
	}


    @Override
    public int numRows() {
        // TODO return the height of the grid
        return rows;  
    }

    @Override
    public int numColumns() {
        // TODO return the width of the grid
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO set the contents of the cell in the given row,column location. 
        // Row must be greater than or equal to 0 and less than numRows().
        // Column must be greater than or equal to 0 and less than numColumns().
        // If invalid values for row or column is passed, an InvallidArgumentException is thrown.
        if (row < 0 || row >= rows || column < 0 || column >= cols) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;
    }

    

    @Override
    // TODO return the contents of the cell in the given row,column location
    // Row must be greater than or equal to 0 and less than numRows().
    // Column must be greater than or equal to 0 and less than numColumns().

    public CellState get(int row, int column) {
        
        if (row < 0 || row >= rows || column < 0 || column >= cols) {
        }
        return grid[row][column];
    }



    @Override
    public IGrid copy() {
        // TODO returns a copy of the grid, with the same elements
        IGrid gridCopy = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                gridCopy.set(i, j, get(i, j));
            }
        }
        return gridCopy;
        
    }
    
}
