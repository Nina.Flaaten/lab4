package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO return the number of rows in this automaton
		// currentGeneration: the grid of cells
		// numRows; the height of the grid
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		// currentGeneration: the grid of cells
		// numRows; the width of the grid
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO return the state of the cell in the given row and column.
		// currentGeneration: the grid of cells
		// get: Get the contents of the cell in the given row,column location, (CellState datastructure.IGrid.get(int row, int column).
		// parameters are row, col
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO updates the state of the cell according to the rules of the automaton
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}

		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO return next state of a cell
		// countNeighbors: calculates the number of neighbors having a given CellState of a cell on position (row, col) on the board.
		// REGLER:
		// En levende celle med færre enn to levende naboer dør.
		// En levende celle med to eller tre levende naboer overlever.
		// En levende celle med mer enn tre levende naboer dør.
		// En død celle med nøyaktig tre levende naboer blir levende.
		
		int livingNeighbor = countNeighbors(row, col, CellState.ALIVE);
		if (livingNeighbor < 2 || livingNeighbor > 3) {
			return CellState.DEAD;
		}
		
		if (livingNeighbor == 3 ) {
			return CellState.ALIVE;
		}
		
		if (getCellState(row, col) == CellState.ALIVE) {
			return CellState.ALIVE;
		}
		else {
			return CellState.DEAD;
		}
	}


	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// TODO return the number of neighbors with given state
		
 
		int neighbors = 0;
		for (int diff = -1; diff < 2; diff++) {
			for (int colDiff = -1; colDiff < 2; colDiff++) {
				if (diff == 0 && colDiff == 0) {
					continue;
				}
				if (row+diff < 0 || row+diff >= numberOfRows()) {
					continue;
				}
				if (col+colDiff < 0 || col+colDiff >= numberOfColumns()) {
					continue;
				}
				if (currentGeneration.get(row+diff, col+colDiff) == state) {
					neighbors++;
				}
			}
		}
		return neighbors;

	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
